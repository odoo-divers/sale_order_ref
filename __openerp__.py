{
    'name' : 'Sale Order Référence in Tree',
    'version' : '0.1',
    'author' : 'Mithril Informatique',
    'sequence': 130,
    'category': '',
    'website' : 'https://www.mithril.re',
    'summary' : '',
    'description' : "",
    'depends' : [
        'base',
        'sale',
    ],
    'data' : [
    		'sale_order_ref.xml',
    ],

    'installable' : True,
    'application' : False,
}
